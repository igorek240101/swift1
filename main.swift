import Foundation;

let isInput = false; //Переключатель: стандартные данные / ручной ввод
let dateFormatter = DateFormatter();
dateFormatter.dateFormat = "dd'.'MM'.'yy'"

// Инициализация данных в зависимости от режима
let person : Human = isInput ?  inputHuman() : Human.init(
                                                name : "Игорь",
                                                birthDate : dateFormatter.date(from : "24.09.2001")!,
                                                university : "ЛЭТИ",
                                                classNumber : 4,
                                                workPlace : "Техническая поддержка",
                                                experience : 1);

print(person.toString()); // Вывод информации о человеке

struct Human
{
    // Поля данных
    let name : String;
    let birthDate : Date;
    let university : String;
    let classNumber : Int;
    let workPlace : String;
    let experience : Int?;
    
    // Вычисление возраста (количества полных лет) по дате рождения
    var age : Int
    {
        let year = Calendar.current.dateComponents([.year], from: Date()).year! - Calendar.current.dateComponents([.year], from: birthDate).year!;
        let month = Calendar.current.dateComponents([.month], from: Date()).month! - Calendar.current.dateComponents([.month], from: birthDate).month!;
        let day = Calendar.current.dateComponents([.day], from: Date()).day! - Calendar.current.dateComponents([.day], from: birthDate).day!;
        return month < 0 || (month == 0 && day < 0) ? year - 1 : year;
    }
    
    // Вычисление возраста на момент поступления в вуз
    var intoUniversity : Int
    {
        let now = Date();
        let prevBithDate : Date = Calendar.current.date(byAdding : DateComponents(year : age), to : birthDate)!
        let year : Int = Calendar.current.dateComponents([.year], from: now).year! - (Calendar.current.dateComponents([.month], from: now).month! < 9 ? 1 : 0);
        let startLerning : Date = dateFormatter.date(from : "01.09.\(year)")!;
        if prevBithDate > startLerning
        {
            return age - classNumber;
        }
        else
        {
            return age - classNumber + 1;
        }
    }
    
    // Перевод информации о человеке в текстовую форму
    func toString() -> String
    {
        return 
        "Имя: \(name)\r\n" +
        "Возраст: \(age) \(Human.ageString(age: age))\r\n" +
        "В \(intoUniversity) \(Human.ageString(age: intoUniversity)) поступил в \(university) (сейчас на \(classNumber) курсе)\r\n" +
        (workPlace != "" ? "Место работы: \(workPlace), опыт работы: \(experience!) \(Human.ageString(age: experience!))" : "Безработный");
    }
    
    // опредление формы слова год / лет в зависимости от количества лет
    static func ageString(age : Int) -> String
    {
        if age % 100 >= 11 && age % 100 <= 14 {return "лет";}
        var result : String;
        switch age % 10 
        {
            case 1: 
                result = "год";
            case 2...4:
                result = "года";
            default:
                result = "лет";
        };
        return result;
    }
}

// Механизм чтения данных о человеке (в случае если выбран режим ручного ввода)
func inputHuman() -> Human
{
    var name : String;
    var birthDate : Date;
    var university : String;
    var classNumber : Int;
    var workPlace : String;
    var experience : Int?;
    print("Привет, как тебя зовут?");
    var input : String? = readLine();
    name = input == nil ? "" : input!;
    print("Когда ты родился? (дд.мм.гггг)");
    input = readLine();
    if input == nil
    {
        birthDate  = Date();
    }
    else
    {
        let date : Date? = dateFormatter.date(from : input!);
        birthDate  = date == nil ? Date() : date!;
    }
    print("В каом вузе ты учишься?");
    input = readLine();
    university = input == nil ? "" : input!;
    print("На каком ты курсе?");
    input = readLine();
    if input == nil
    {
        classNumber = 0;
    }
    else
    {
        let number : Int? = Int(input!);
        classNumber = number == nil ? 0 : number!;
    }
    print("Кем ты работаешь? (если ещё не работаешь оставь пустым)");
    input = readLine();
    workPlace = input == nil ? "" : input!;
    if workPlace != ""
    {
        print("Какой у тебя стаж?");
        input = readLine();
        if input == nil
        {
            experience = 0;
        }
        else
        {
            let number : Int? = Int(input!);
            experience = number == nil ? 0 : number!;
        }
    }
    return Human.init(
        name : name,
        birthDate : birthDate,
        university : university,
        classNumber : classNumber,
        workPlace : workPlace,
        experience : experience
    );
}
